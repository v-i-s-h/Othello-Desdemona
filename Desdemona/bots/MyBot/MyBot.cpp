/*
* @file botTemplate.cpp
* @author Arun Tejasvi Chaganty <arunchaganty@gmail.com>
* @date 2010-02-04
* Template for users to create their own bots
*
*/

#include "Othello.h"
#include "OthelloBoard.h"
#include "OthelloPlayer.h"
#include <iostream>
#include <cstdlib>
#include <iomanip>
using namespace std;
using namespace Desdemona;

#define BOARD_SIZE 8

int pointDist[BOARD_SIZE][BOARD_SIZE] = {
    { 9, 6, 6, 6, 6, 6, 6, 9 },
    { 6, 3, 3, 3, 3, 3, 3, 6 },
    { 6, 3, 2, 2, 2, 2, 3, 6 },
    { 6, 3, 2, 1, 1, 2, 3, 6 },
    { 6, 3, 2, 1, 1, 2, 3, 6 },
    { 6, 3, 2, 2, 2, 2, 3, 6 },
    { 6, 3, 3, 3, 3, 3, 3, 6 },
    { 9, 6, 6, 6, 6, 6, 6, 9 },
};

class MyBot: public OthelloPlayer
{
    public:
        /**
         * Initialisation routines here
         * This could do anything from open up a cache of "best moves" to
         * spawning a background processing thread.
         */
        MyBot( Turn turn );

        /**
         * Play something
         */
        virtual Move play( const OthelloBoard& board );
    private:
        unsigned int noOfSteps;
        Coin opponentCoin;

        /*
        * Evaluation function: return a INTEGER
        */
        int evalBoard( const OthelloBoard& board );

};

MyBot::MyBot( Turn turn )
    : OthelloPlayer( turn )
{
    noOfSteps = 0;
    if( turn == BLACK ) {
        opponentCoin = RED;
    } else {
        opponentCoin = BLACK;
    }
    // cout << "My Coin: " << ((turn==BLACK)?"X":"O") << "    "
    //      << "Opponent: " << ((opponentCoin==BLACK)?"X":"O")
    //      << endl;
}

Move MyBot::play( const OthelloBoard& board )
{
    list<Move> moves = board.getValidMoves( turn );
    list<Move>::iterator bestMove = moves.begin();
    noOfSteps++;
    // cout << "Step#" << noOfSteps << endl;
    // board.print( turn );
    // int boardValue = evalBoard( board );
    // cout << "Value: " << boardValue << endl;
    // cout << "Valid Moves: " << endl;
    // for first move
    OthelloBoard tmpBoard = board;
    tmpBoard.makeMove( turn, *bestMove );
    int newVal = evalBoard( tmpBoard );
    // cout << "    (" << bestMove->x << "," << bestMove->y << ") -> " << newVal << endl;
    int bestMoveVal = newVal;
    for( list<Move>::iterator it = ++moves.begin(); it != moves.end(); it++ ) {
        tmpBoard = board;
        tmpBoard.makeMove( turn, *it );
        newVal = evalBoard( tmpBoard );
        // cout << "    (" << it->x << "," << it->y << ") -> " << newVal << endl;
        if( newVal > bestMoveVal ) {
            bestMoveVal = newVal;
            bestMove = it;
        } else if( newVal == bestMoveVal ) {
            // toss an decide
            if( rand() > 0.5 ) {
                bestMove = it;
            }
        }
        // Evaluate that move
    }
    // cout << "[MYBOT]: " << "#" << noOfSteps 
    //      << "    (" << bestMove->x << "," << bestMove->y << ")" << endl;
    return *bestMove;
}

int MyBot::evalBoard( const OthelloBoard& board ) {
    int value = 0;
    for( unsigned i = 0; i < BOARD_SIZE; i++ ) {
        for( unsigned j = 0; j < BOARD_SIZE; j++ ) {
            Coin thisCoin = board.get(i,j);
            if( thisCoin == turn ) {  // whether my coin
                value += pointDist[i][j];
            } else if( thisCoin == opponentCoin ) {
                value -= pointDist[i][j];
            }
        }
    }
    // if we can eliminate all coins of the Opponent
    int noOfOppCoins = (opponentCoin==BLACK)?board.getBlackCount():board.getRedCount();
    value += (noOfOppCoins==0)?100:0;
    return value;
}

// The following lines are _very_ important to create a bot module for Desdemona

extern "C" {
    OthelloPlayer* createBot( Turn turn )
    {
        return new MyBot( turn );
    }

    void destroyBot( OthelloPlayer* bot )
    {
        delete bot;
    }
}
