/*
* @file botTemplate.cpp
* @author Arun Tejasvi Chaganty <arunchaganty@gmail.com>
* @date 2010-02-04
* Template for users to create their own bots
*
*/

#include "Othello.h"
#include "OthelloBoard.h"
#include "OthelloPlayer.h"
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <iomanip>

using namespace std;
using namespace Desdemona;

#define BOARD_SIZE  8
#define PLY_DEPTH   4

// #define ENABLE_DEBUG
#define DEBUG_PRIO_1
#define DEBUG_PRIO_5
#define DEBUG_PRIO_9

// int pointDist[BOARD_SIZE][BOARD_SIZE] = {
//     { 9, 6, 6, 6, 6, 6, 6, 9 },
//     { 6, 1, 2, 2, 2, 2, 1, 6 },
//     { 6, 2, 5, 5, 5, 5, 2, 6 },
//     { 6, 2, 5, 1, 1, 5, 2, 6 },
//     { 6, 2, 5, 1, 1, 5, 2, 6 },
//     { 6, 2, 5, 5, 5, 5, 2, 6 },
//     { 6, 1, 2, 2, 2, 2, 1, 6 },
//     { 9, 6, 6, 6, 6, 6, 6, 9 },
// };

int pointDist[BOARD_SIZE][BOARD_SIZE] = {
    {  120,  -20,   20,    5,    5,   20,  -20,  120 },
    {  -20,  -40,   -5,   -5,   -5,   -5,  -40,  -20 },
    {   20,   -5,   15,    3,    3,   15,   -5,   20 },
    {    5,    -5,   3,    3,    3,    3,   -5,    5 },
    {    5,    -5,   3,    3,    3,    3,   -5,    5 },
    {   20,   -5,   15,    3,    3,   15,   -5,   20 },
    {  -20,  -40,   -5,   -5,   -5,   -5,  -40,  -20 },
    {  120,  -20,   20,    5,    5,   20,  -20,  120 },
};

class MiniMax: public OthelloPlayer
{
    public:
        /**
         * Initialisation routines here
         * This could do anything from open up a cache of "best moves" to
         * spawning a background processing thread.
         */
        MiniMax( Turn turn );

        /**
         * Play something
         */
        virtual Move play( const OthelloBoard& board );
    private:
        unsigned int noOfSteps;
        Coin opponentCoin;
        int largeVal;

        /*
        * Evaluation function: return a INTEGER
        */
        int evalBoard( const OthelloBoard& board );

        /*
        *   AlphaBeta algorithm
        */
        int minimax( const OthelloBoard& board,
                        Coin player,
                        unsigned plyDepth );

        /*
         *  Debug Utility
         */
        void printBoard( const OthelloBoard& board, unsigned level = 0 );

};

MiniMax::MiniMax( Turn turn )
    : OthelloPlayer( turn )
{
    noOfSteps = 0;
    if( turn == BLACK ) {
        opponentCoin = RED;
    } else {
        opponentCoin = BLACK;
    }
    // cout << "My Coin: " << ((turn==BLACK)?"X":"O") << "    "
    //      << "Opponent: " << ((opponentCoin==BLACK)?"X":"O")
    //      << endl;
    // find the LARGE value for setting
    largeVal = 100;
    for( unsigned _i = 0; _i < BOARD_SIZE; _i++ ) {
        for( unsigned _j = 0; _j < BOARD_SIZE; _j++ ) {
            largeVal += pointDist[_i][_j];
        }
    }
}

Move MiniMax::play( const OthelloBoard& board ) {
    noOfSteps++;    // for internal tracking

    list<Move> validMoves   = board.getValidMoves( turn );
    list<Move>::iterator bestMove = validMoves.begin();

    OthelloBoard tmpBoard   = board;
    int bestMoveVal         = -largeVal;

#if defined(ENABLE_DEBUG) && defined(DEBUG_PRIO_9)
    cout << "[MiniMax::Play]: "
         << "Evaluating board configuration" << endl;
    printBoard( board, 0 );
#endif

    for( list<Move>::iterator mvItr = validMoves.begin(); 
            mvItr != validMoves.end();
            mvItr++ ) {
        // generate child 
        tmpBoard = board;
        tmpBoard.makeMove( turn, *mvItr );
        // calculate AlphaBeta value
        int thisMoveVal = minimax( tmpBoard, opponentCoin, PLY_DEPTH-1 );
#if defined(ENABLE_DEBUG) && defined(DEBUG_PRIO_1)
        cout << "[MiniMax::Play]: "
             << "Value = " << thisMoveVal << " "
             << "@(" << mvItr->x << "," << mvItr->y << ")" << endl;
#endif
        // check against current best move and update
        if( thisMoveVal > bestMoveVal ) {
            bestMoveVal = thisMoveVal;
            bestMove    = mvItr;
        } 
    }
#if defined(ENABLE_DEBUG) && defined(DEBUG_PRIO_5)
    cout << "[MiniMax::Play]: "
         << "[" << noOfSteps  << "]: "
         << "Playing @(" << bestMove->x << "," << bestMove->y << ")"
         << " with value = " << bestMoveVal
         << endl;
#endif
    
    return *bestMove;
}

int MiniMax::evalBoard( const OthelloBoard& board ) {
    int value = 0;
    for( unsigned i = 0; i < BOARD_SIZE; i++ ) {
        for( unsigned j = 0; j < BOARD_SIZE; j++ ) {
            Coin thisCoin = board.get(i,j);
            if( thisCoin == turn ) {  // whether my coin
                value += pointDist[i][j];
            } else if( thisCoin == opponentCoin ) {
                value -= pointDist[i][j];
            }
        }
    }
    // if we can eliminate all coins of the Opponent
    int noOfOppCoins = (opponentCoin==BLACK)?board.getBlackCount():board.getRedCount();
    value += ((noOfOppCoins==0)?242:0);
    // if we gets eliminated
    int noOfSelfCoins   = (turn==BLACK)?board.getBlackCount():board.getRedCount();
    value += ((noOfSelfCoins==0)?(-242):0);
    return value;
}

int MiniMax::minimax( const OthelloBoard& board,
                            Coin player, unsigned plyDepth )
{
#if defined(ENABLE_DEBUG) && defined(DEBUG_PRIO_5)
    for( unsigned _i = 0; _i < PLY_DEPTH-plyDepth; _i++ )
        cout << "    ";
    cout << "[MiniMax::MiniMax]: @depth = " << PLY_DEPTH-plyDepth << "  >>> " << ((player==turn)?"MAX":"MIN") << endl;
#endif
#if defined(ENABLE_DEBUG) && defined(DEBUG_PRIO_9)
    for( unsigned _i = 0; _i < PLY_DEPTH-plyDepth; _i++ )
        cout << "    ";
    cout << "[MiniMax::MiniMax]: Evaluating board configuration" << endl;
    printBoard( board, PLY_DEPTH-plyDepth );
#endif

    if( plyDepth == 0 ) {
        int boardConfigVal  = evalBoard( board );
#if defined(ENABLE_DEBUG) && defined(DEBUG_PRIO_5)
    for( unsigned _i = 0; _i < PLY_DEPTH-plyDepth; _i++ )
        cout << "    ";
    cout << "[MiniMax::MiniMax]: "
         << "calculated board value = " << boardConfigVal << endl;
#endif
        return boardConfigVal;
    }

    // else
    list<Move> validMoves   = board.getValidMoves( (player==turn)?turn:opponentCoin );
    
    OthelloBoard tmpBoard   = board;

    int selectedMoveVal     = 0;

    for( list<Move>::iterator mvItr = validMoves.begin(); 
            mvItr != validMoves.end();
            mvItr++ ) {
        // generate child
        tmpBoard = board;
        tmpBoard.makeMove( ((player==turn)?turn:opponentCoin), *mvItr );
        // calculate alphabeta value
        int thisMoveVal = minimax( tmpBoard,
                                   (player==opponentCoin)?turn:opponentCoin,
                                   plyDepth-1 );
        if( mvItr == validMoves.begin() ) {
            selectedMoveVal = thisMoveVal;
        }
        if( player == turn ) {  // MAX level
            selectedMoveVal = (thisMoveVal>selectedMoveVal)?thisMoveVal:selectedMoveVal;
        } else {                // MIN level
            selectedMoveVal = (thisMoveVal<selectedMoveVal)?thisMoveVal:selectedMoveVal;
        }
        
    }
#if defined(ENABLE_DEBUG) && defined(DEBUG_PRIO_5)
    for( unsigned _i = 0; _i < PLY_DEPTH-plyDepth; _i++ )
        cout << "    ";
    cout << "[MiniMax::MiniMax]: "
         << "Calculated Value = " << selectedMoveVal << endl;
#endif

    return selectedMoveVal;
}

void MiniMax::printBoard( const OthelloBoard& board, unsigned level ) {
    for( unsigned _k = 0; _k < level; _k++ )
        cout << "    ";
    cout << "=================" << endl;
    for( unsigned _i = 0; _i < BOARD_SIZE; _i++ ) {
        for( unsigned _k = 0; _k < level; _k++ )
            cout << "    ";
        cout << "|";
        for( unsigned _j = 0; _j < BOARD_SIZE; _j++ ) {
            cout << ((board.get(_i,_j)==BLACK)?"X":(board.get(_i,_j)==RED)?"O":" ") << "|";
        }
        cout << endl;
    }
    for( unsigned _k = 0; _k < level; _k++ )
        cout << "    ";
    cout << "=================" << endl;
}

// The following lines are _very_ important to create a bot module for Desdemona

extern "C" {
    OthelloPlayer* createBot( Turn turn )
    {
        return new MiniMax( turn );
    }

    void destroyBot( OthelloPlayer* bot )
    {
        delete bot;
    }
}
