/*
* @file botTemplate.cpp
* @author Arun Tejasvi Chaganty <arunchaganty@gmail.com>
* @date 2010-02-04
* Template for users to create their own bots
*
*/

#include "Othello.h"
#include "OthelloBoard.h"
#include "OthelloPlayer.h"
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <iomanip>

using namespace std;
using namespace Desdemona;

#define BOARD_SIZE 8

int pointDist[BOARD_SIZE][BOARD_SIZE] = {
    { 9, 6, 6, 6, 6, 6, 6, 9 },
    { 6, 1, 2, 2, 2, 2, 1, 6 },
    { 6, 2, 5, 5, 5, 5, 2, 6 },
    { 6, 2, 5, 1, 1, 5, 2, 6 },
    { 6, 2, 5, 1, 1, 5, 2, 6 },
    { 6, 2, 5, 5, 5, 5, 2, 6 },
    { 6, 1, 2, 2, 2, 2, 1, 6 },
    { 9, 6, 6, 6, 6, 6, 6, 9 },
};

// int pointDist[BOARD_SIZE][BOARD_SIZE] = {
//     {  120,  -20,   20,    5,    5,   20,  -20,  120 },
//     {  -20,  -40,   -5,   -5,   -5,   -5,  -40,  -20 },
//     {   20,   -5,   15,    3,    3,   15,   -5,   20 },
//     {    5,    -5,   3,    3,    3,    3,   -5,    5 },
//     {    5,    -5,   3,    3,    3,    3,   -5,    5 },
//     {   20,   -5,   15,    3,    3,   15,   -5,   20 },
//     {  -20,  -40,   -5,   -5,   -5,   -5,  -40,  -20 },
//     {  120,  -20,   20,    5,    5,   20,  -20,  120 },
// };

class AlphaBetaBot: public OthelloPlayer
{
    public:
        /**
         * Initialisation routines here
         * This could do anything from open up a cache of "best moves" to
         * spawning a background processing thread.
         */
        AlphaBetaBot( Turn turn );

        /**
         * Play something
         */
        virtual Move play( const OthelloBoard& board );
    private:
        unsigned int noOfSteps;
        Coin opponentCoin;
        int largeVal;

        /*
        * Evaluation function: return a INTEGER
        */
        int evalBoard( const OthelloBoard& board );

        /*
        *   AlphaBeta algorithm
        */
        int alphaBeta( const OthelloBoard& board,
                        int &alpha,
                        int &beta,
                        Coin player,
                        unsigned plyDepth );

        /*
         *  Debug Utility
         */
        void printBoard( const OthelloBoard& board, unsigned level = 0 );

};

AlphaBetaBot::AlphaBetaBot( Turn turn )
    : OthelloPlayer( turn )
{
    noOfSteps = 0;
    if( turn == BLACK ) {
        opponentCoin = RED;
    } else {
        opponentCoin = BLACK;
    }
    // cout << "My Coin: " << ((turn==BLACK)?"X":"O") << "    "
    //      << "Opponent: " << ((opponentCoin==BLACK)?"X":"O")
    //      << endl;
    // find the LARGE value for setting
    largeVal = 100;
    for( unsigned _i = 0; _i < BOARD_SIZE; _i++ ) {
        for( unsigned _j = 0; _j < BOARD_SIZE; _j++ ) {
            largeVal += pointDist[_i][_j];
        }
    }
}

Move AlphaBetaBot::play( const OthelloBoard& board ) {
    noOfSteps++;    // for internal tracking

    list<Move> validMoves   = board.getValidMoves( turn );
    list<Move>::iterator bestMove = validMoves.begin();

    OthelloBoard tmpBoard   = board;
    int bestMoveVal         = -largeVal;
    int alphaValue          = -largeVal;
    int betaValue           = +largeVal;
    for( list<Move>::iterator mvItr = validMoves.begin(); 
            mvItr != validMoves.end();
            mvItr++ ) {
        // generate child 
        tmpBoard = board;
        tmpBoard.makeMove( turn, *mvItr );
        // calculate AlphaBeta value
        int thisMoveVal = alphaBeta( tmpBoard, alphaValue, betaValue,
                                        opponentCoin, 2 );
        // check against current best move and update
        if( thisMoveVal > bestMoveVal ) {
            bestMoveVal = thisMoveVal;
            bestMove    = mvItr;
        } 
    }

    cout << "[AlphaBetaBot]: " << "#" << noOfSteps 
         << "     " << bestMoveVal
         << " @(" << bestMove->x << "," << bestMove->y << ")" << endl;
    
    return *bestMove;
}

int AlphaBetaBot::evalBoard( const OthelloBoard& board ) {
    int value = 0;
    for( unsigned i = 0; i < BOARD_SIZE; i++ ) {
        for( unsigned j = 0; j < BOARD_SIZE; j++ ) {
            Coin thisCoin = board.get(i,j);
            if( thisCoin == turn ) {  // whether my coin
                value += pointDist[i][j];
            } else if( thisCoin == opponentCoin ) {
                value -= pointDist[i][j];
            }
        }
    }
    // if we can eliminate all coins of the Opponent
    int noOfOppCoins = (opponentCoin==BLACK)?board.getBlackCount():board.getRedCount();
    value += (noOfOppCoins==0)?100:0;
    return value;
}

int AlphaBetaBot::alphaBeta( const OthelloBoard& board,
                              int &alpha, int &beta, Coin player, unsigned plyDepth )
{
    for( int _i = 0; _i < 3-plyDepth; _i++ )
        cout << "    ";
    cout << "    [alphaBeta]: @" << plyDepth << "    "
         << "alpha = " << alpha << "    "
         << "beta = " << beta << endl;

    if( plyDepth == 0 ) {
        return evalBoard( board );
    }

    // else
    list<Move> validMoves = board.getValidMoves( turn );
    
    OthelloBoard tmpBoard = board;

    for( list<Move>::iterator mvItr = validMoves.begin(); 
            mvItr != validMoves.end();
            mvItr++ ) {
        // generate child
        tmpBoard = board;
        tmpBoard.makeMove( turn, *mvItr );
        // calculate alphabeta value
        int thisMoveVal = alphaBeta( tmpBoard, alpha, beta, 
                                        (player==opponentCoin)?turn:opponentCoin,
                                        plyDepth-1);
        if( player == opponentCoin ) {  // MIN level
            beta = ((beta>thisMoveVal)?thisMoveVal:beta);
            if( alpha >= beta ) {
                return alpha;
            }
            if( mvItr == validMoves.end() ) {
                return beta;
            }
        } else {                        // MAX level
            alpha = ((alpha<thisMoveVal)?thisMoveVal:alpha);
            if( alpha >= beta ) {
                return beta;
            } 
            if( mvItr == validMoves.end() ) {
                return alpha;
            }
        }
        // update 'alpha' and 'beta'
    }

    return evalBoard( board );
    
}

void AlphaBetaBot::printBoard( const OthelloBoard& board, unsigned level ) {
    for( unsigned _k = 0; _k < level; _k++ )
        cout << "    ";
    cout << "=================" << endl;
    for( unsigned _i = 0; _i < BOARD_SIZE; _i++ ) {
        for( unsigned _k = 0; _k < level; _k++ )
            cout << "    ";
        cout << "|";
        for( unsigned _j = 0; _j < BOARD_SIZE; _j++ ) {
            cout << ((board.get(_i,_j)==BLACK)?"X":(board.get(_i,_j)==RED)?"O":" ") << "|";
        }
        cout << endl;
    }
    for( unsigned _k = 0; _k < level; _k++ )
        cout << "    ";
    cout << "=================" << endl;
}

// The following lines are _very_ important to create a bot module for Desdemona

extern "C" {
    OthelloPlayer* createBot( Turn turn )
    {
        return new AlphaBetaBot( turn );
    }

    void destroyBot( OthelloPlayer* bot )
    {
        delete bot;
    }
}
